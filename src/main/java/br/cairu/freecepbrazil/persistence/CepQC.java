package br.cairu.freecepbrazil.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.cairu.freecepbrazil.to.CepTO;

@Repository
public class CepQC extends BaseQC {

	@SuppressWarnings("unchecked")
	public List<HashMap<String, String>> testarConexaoBanco(){
		List<HashMap<String, String>> retorno = new ArrayList<HashMap<String, String>>();
		final String sql = "select * from teste_1";
		Query query = manager.createNativeQuery(sql);
		List<Object[]> resultados = query.getResultList();
		for(Object[] resultado : resultados) {
			HashMap<String, String> mapResultado = new HashMap<String, String>();
			mapResultado.put(retornarValor(resultado[1]), retornarValor(resultado[2]));
			retorno.add(mapResultado);
		}
		return retorno;
	}

	public CepTO consultarCep(String cep) {
		final String sql = "SELECT * FROM LOGRADOURO WHERE CEP = ?";
		Query query = manager.createNativeQuery(sql);
		query.setParameter(1, cep);
		Object[] resultado = (Object[]) query.getResultList().get(0);
		if(Objects.nonNull(resultado)) {
			CepTO cepTO = new CepTO();
			cepTO.setCep(retornarValorInt(resultado[0]));
			cepTO.setIdLogradouro(retornarValorInt(resultado[1]));
			cepTO.setTipo(retornarValor(resultado[2]));
			cepTO.setDescricao(retornarValor(resultado[3]));
			cepTO.setIdCidade(retornarValorInt(resultado[4]));
			cepTO.setUf(retornarValor(resultado[5]));
			cepTO.setComplemento(retornarValor(resultado[6]));
			cepTO.setDescricaoSemNumero(retornarValor(resultado[7]));
			cepTO.setDescricaoCidade(retornarValor(resultado[8]));
			cepTO.setCodigoCidadeIBGE(retornarValorInt(resultado[9]));
			cepTO.setDescricaoBairro(retornarValor(resultado[10]));
			return cepTO;
		} else {
			return null;
		}
	}
	
	private int retornarValorInt(Object objeto) {
		return Objects.nonNull(objeto) ? Integer.parseInt(objeto.toString()) : 0;
	}

	private String retornarValor(Object objeto) {
		return Objects.nonNull(objeto) ? objeto.toString() : "";
	}

}

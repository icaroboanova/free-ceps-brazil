package br.cairu.freecepbrazil.persistence;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class BaseQC {
	
	@PersistenceContext
    public EntityManager manager;

}

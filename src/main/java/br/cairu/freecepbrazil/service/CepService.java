package br.cairu.freecepbrazil.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.cairu.freecepbrazil.persistence.CepQC;
import br.cairu.freecepbrazil.to.CepTO;

@Service
public class CepService {
	
	@Autowired
	private CepQC cepQC;
	
	public List<HashMap<String, String>> testarConexaoBanco(){
		return cepQC.testarConexaoBanco();
	}
	
	
	public CepTO consultarCep(String cep) {
		return cepQC.consultarCep(cep);
	}


}

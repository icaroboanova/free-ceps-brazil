package br.cairu.freecepbrazil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreeCepBrazilApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreeCepBrazilApplication.class, args);
	}

}

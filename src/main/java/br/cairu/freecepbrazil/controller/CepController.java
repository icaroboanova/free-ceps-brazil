package br.cairu.freecepbrazil.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.cairu.freecepbrazil.service.CepService;
import br.cairu.freecepbrazil.to.CepTO;

@Component
@Controller
@CrossOrigin
@RequestMapping("/api/cep")
@RestController
public class CepController {
	
	@Autowired
	private CepService cepService;
	
	@CrossOrigin
	@GetMapping("/{cep}")
	public ResponseEntity<CepTO> buscarCep(@PathVariable String cep){ 
		return new ResponseEntity<CepTO>(cepService.consultarCep(cep), HttpStatus.OK);
	}

}

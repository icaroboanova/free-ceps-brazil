package br.cairu.freecepbrazil.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.cairu.freecepbrazil.service.CepService;

@Component
@Controller
@CrossOrigin
@RequestMapping("/api/teste")
@RestController
public class TestController {

	@Autowired
	private CepService cepService;
	
	@CrossOrigin
	@GetMapping("/hello")
	public ResponseEntity<String> helloWorld(){ 
		return new ResponseEntity<String>("Hello World! grupo de gestão estrategica", HttpStatus.ACCEPTED);
	}
	
	@CrossOrigin
	@GetMapping("/testeConexao")
	public ResponseEntity<List<HashMap<String, String>>> testarConexaoBanco(){ 
		return new ResponseEntity<List<HashMap<String, String>>>(cepService.testarConexaoBanco(), HttpStatus.ACCEPTED);
	}
	
}

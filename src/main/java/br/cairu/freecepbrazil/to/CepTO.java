package br.cairu.freecepbrazil.to;

import org.springframework.stereotype.Component;

@Component
public class CepTO {

    private int cep;
    private int idLogradouro;
    private String tipo;
    private String descricao;
    private int idCidade;
    private String uf;
    private String complemento;
    private String descricaoSemNumero;
    private String descricaoCidade;
    private int codigoCidadeIBGE;
    private String descricaoBairro;
    
    
	public int getCep() {
		return cep;
	}
	public void setCep(int cep) {
		this.cep = cep;
	}
	public int getIdLogradouro() {
		return idLogradouro;
	}
	public void setIdLogradouro(int idLogradouro) {
		this.idLogradouro = idLogradouro;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getIdCidade() {
		return idCidade;
	}
	public void setIdCidade(int idCidade) {
		this.idCidade = idCidade;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getDescricaoSemNumero() {
		return descricaoSemNumero;
	}
	public void setDescricaoSemNumero(String descricaoSemNumero) {
		this.descricaoSemNumero = descricaoSemNumero;
	}
	public String getDescricaoCidade() {
		return descricaoCidade;
	}
	public void setDescricaoCidade(String descricaoCidade) {
		this.descricaoCidade = descricaoCidade;
	}
	public int getCodigoCidadeIBGE() {
		return codigoCidadeIBGE;
	}
	public void setCodigoCidadeIBGE(int codigoCidadeIBGE) {
		this.codigoCidadeIBGE = codigoCidadeIBGE;
	}
	public String getDescricaoBairro() {
		return descricaoBairro;
	}
	public void setDescricaoBairro(String descricaoBairro) {
		this.descricaoBairro = descricaoBairro;
	}
    
}
